program euler
    use procedures

    integer, dimension(:), allocatable :: results

    block
        INTEGER :: n
        write (*, *) "n = "
        read (*, *) n
        allocate (results(n))
    end block
    block
        INTEGER :: i
        do i = 1, size(results)
            results(i) = i
        end do
    end block

    block
        INTEGER :: total_primes
        logical, dimension(:), allocatable :: isPrime
        integer, dimension(:), allocatable :: primes

        allocate (isPrime(size(results)))
        isPrime = .true. ! every number is prime unless proved otherwise

        call set_primes(primes, isPrime, total_primes)
        call update_results(results, primes, total_primes)
    end block

    ! do i=1, size(results)
    !     write(*, '(i3)', advance='no') results(i)
    ! end do
    write (*, '(100i3)', advance='no') results
end program euler

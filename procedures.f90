module procedures
    implicit none
contains
    subroutine set_primes(primes, isPrime, total_primes)
        integer, dimension(:), INTENT(OUT), ALLOCATABLE :: primes
        logical, dimension(:), INTENT(INOUT) :: isPrime
        INTEGER, INTENT(OUT) :: total_primes

        total_primes = 0
        block
            INTEGER :: i
            integer, dimension(:), allocatable :: primes_helper
            ALLOCATE (primes_helper(size(isPrime)))
            do i = 2, size(isPrime)
                if (isPrime(i)) then
                    total_primes = total_primes + 1
                    primes_helper(total_primes) = i
                    block
                        INTEGER :: j
                        do j = 2*i, size(isPrime), i
                            isPrime(j) = .false.
                        end do
                    end block
                end if
            end do
            allocate (primes(total_primes))
            primes = primes_helper(:total_primes)
        end block
    end subroutine set_primes

    subroutine update_results(results, primes, total_primes)
        !dummy arguments
        integer, dimension(:), INTENT(OUT) :: results
        integer, dimension(:), INTENT(IN) :: primes
        integer, INTENT(IN) :: total_primes

        block
            integer :: i
            do i = 1, total_primes
                block
                    INTEGER :: p
                    p = primes(i)
                    results(p) = p - 1
                    block
                        INTEGER :: j
                        do j = 2*p, size(results), p
                            results(j) = (results(j)/p)*(p - 1)
                        end do
                    end block
                end block
            end do
        end block
    end subroutine update_results

end module procedures

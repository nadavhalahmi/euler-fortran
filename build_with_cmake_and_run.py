import os
import shutil

if os.path.exists('build'):
    shutil.rmtree('build', ignore_errors=True)

os.mkdir('build')
os.chdir('build')
os.system('cmake .. -DCMAKE_PREFIX_PATH=$PFUNIT_DIR')
os.system('make')
os.system('./my_tests')

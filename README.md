## Synopsis

This code should calculate the first n values of Euler's totient function and demonstrate the usage of pFUnit as a unit test environment of Fortran.

## Clone

If one wants to clone the recent version of branch `master` of the project - one needs to clone it from `GitLab` using:

```git
git clone https://gitlab.com/nadavhalahmi/euler-fortran.git
```

## Running euler.f90

If one wants to run this project's main file - one needs to compile euler.f90 and then run the executable file:

```
cd euler-fortran
gfortran euler.f90 procedures.f90 -o euler
./euler
```

## Running tests

If one wants to run the tests of this project - one needs first to configure pFUnit using:

```git
git clone https://github.com/Goddard-Fortran-Ecosystem/pFUnit.git
cd pFUnit
export FC=gfortran
mkdir build
cd build
cmake ..
make tests #this may take a few minutes
make install
cd ../..
```

After counfiguring pFUnit, one can run tests using:

```
cd euler-fortran
export PFUNIT_DIR=../pFUnit/build/installed #can be replaced with absolute path
python3 build_with_cmake_and_run.py
```
